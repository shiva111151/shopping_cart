app.controller('homeC', ['$scope', '$location', '$state', '$http', '$rootScope', '$window', function ($scope, $location, $state, $http, $rootScope, $window) {
    if(!$window.sessionStorage.getItem('User') || JSON.parse(!$window.sessionStorage.getItem('User')).t || JSON.parse(!$window.sessionStorage.getItem('User')).id){
        $location.url("login")
    }
    $scope.uT = JSON.parse($window.sessionStorage.getItem('User')).t;
    var userId = JSON.parse($window.sessionStorage.getItem('User')).id;
    
    $scope.allcart = [];
    $scope.getItems = function () {
        var url = "/server/getItemsall";
        $http({
            method: 'post',
            url: url
        }).then(function successCallback(response) {
            if (response.data === 'invalid input') {
                $scope.error = "invalid input";
            }
            else if (response.data === "db error") {
                $scope.error = "Try Again Something went Wrong";
            }
            else if (response.status === 200) {
                $scope.allData = response.data.d;
            }
        })
    }
    $scope.cat = '';
    $scope.name = '';
    $scope.price = '';
    $scope.descr = '';
    $scope.allE = '';
    $scope.addnewitem = function () {
        if (!$scope.cat || !$scope.name || !$scope.price) {
            $scope.allE = "Please Enter all required fields"
        }
        else {
            let ob = {
                own_Id: userId,
                cat: $scope.cat,
                name: $scope.name,
                price: parseInt($scope.price),
                descr: $scope.descr
            }
            var url = "/server/addItem";
            $http({
                method: 'POST',
                url: url,
                params: {
                    item: ob
                }
            }).then(function successCallback(response) {
                if (response.data === 'invalid input') {
                    $scope.error = "invalid input";
                }
                else if (response.data === "db error") {
                    $scope.error = "Try Again Something went Wrong";
                }
                else if (response.status === 200) {
                    $scope.getItems();
                    $('#myModal').modal('hide');
                }
            })
        }
    }
    
    $scope.addtocart = function (item) {
        let obj = { 'item': item._id, 'u': userId }
        var url = "/server/addtoCart";
        $http({
            method: 'POST',
            url: url,
            params: {
                item: obj
            }
        }).then(function successCallback(response) {
            if (response.data === 'invalid input') {
                $scope.error = "invalid input";
            }
            else if (response.data === "db error") {
                $scope.error = "Try Again Something went Wrong";
            }
            else if (response.status === 200) {
                $scope.getItems();
                $scope.getcartItems();
            }
        })
    }
    $scope.getcartItems = function () {
        var url = "/server/getcartItems";
        $http({
            method: 'post',
            url: url,
            params: {
                id: userId
            }
        }).then(function successCallback(response) {
            if (response.data === 'invalid input') {
                $scope.error = "invalid input";
            }
            else if (response.data === "db error") {
                $scope.error = "Try Again Something went Wrong";
            }
            else if (response.status === 200) {
                $scope.allcart = response.data.d;
            }
        })
    }



    $scope.logout = function () {
        $window.sessionStorage.removeItem("User");
        $location.url('login')
    }
}]);